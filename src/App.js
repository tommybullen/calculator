import React, { useEffect, useState } from "react";

function App() {
  const [input, setInput] = useState("");
  const [op, setOp] = useState("");
  const [total, setTotal] = useState(null);
  const [display, setDisplay] = useState("");
  // const [equal, setEqual] = useState(false)

  //triggered by any number key, pushes digit to input array
  const inputPush = (digit) => {
    setInput(input + digit);
    setDisplay(display + digit);
  };

  // triggered by pressing equals
  const equals = () => {
    const total = getNumbers();
    setDisplay(total);
    setInput(total);
  };

  //triggered by pressing op key, updates op, works out number then triggers calc func
  const getNumbers = (opSign) => {
    setOp(opSign);
    setDisplay(display + opSign);
    var output = Number(input);
    setInput("");
    if (total === null) {
      setTotal(output);
    } else {
      return calc(output);
    }
  };

  //does calculation based on output, total and op, then updates running total with this value
  const calc = (output) => {
    var tempTotal = total;
    op === "+"
      ? (tempTotal += output)
      : op === "-"
      ? (tempTotal -= output)
      : op === "*"
      ? (tempTotal *= output)
      : op === "/"
      ? (tempTotal /= output)
      : (tempTotal = output);
    setTotal(tempTotal);
    return tempTotal;
  };

  const clear = () =>{
    setInput("");
    setOp("");
    setTotal(null);
    setDisplay("");
  }

  return (
    <div className="calculator">
    <div className="buttons">
      <button onClick={(d) => inputPush(7)}>7</button>
      <button onClick={(d) => inputPush(8)}>8</button>
      <button onClick={(d) => inputPush(9)}>9</button>
      <button className="op" id="C" onClick={clear}>C</button>
      <button onClick={(d) => inputPush(4)}>4</button>
      <button onClick={(d) => inputPush(5)}>5</button>
      <button onClick={(d) => inputPush(6)}>6</button>
      <button className="op" onClick={(d) => getNumbers("+")}>&#43;</button>
      <button onClick={(d) => inputPush(1)}>1</button>
      <button onClick={(d) => inputPush(2)}>2</button>
      <button onClick={(d) => inputPush(3)}>3</button>
      <button className="op" onClick={(d) => getNumbers("-")}>&#8722;</button>
      <button onClick={(d) => inputPush(0)}>0</button>
      <button className="op" onClick={(d) => getNumbers("*")}>&#215;</button>
      <button className="op" onClick={(d) => getNumbers("/")}>&#247;</button>
      <button className="op" onClick={equals}>=</button>
      
    </div>
    <p>{display}</p>
    </div>
  );
}

export default App;

